package main

import (
	"example_api/app"

	"github.com/gin-gonic/gin"
)

// main godoc
// @title Тестовое API
// @version 0.1
func main() {
	router := gin.Default()

	storage := &app.Storage{
		Players: make(map[string]app.Player),
	}

	api := app.Api{
		Router:  router,
		Storage: storage,
	}

	api.Start()
}
