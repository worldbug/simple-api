package app

// Player ...
type Player struct {
	Nick string `json:"nick"`
	Age  int    `json:"age"`
	Auth
}

// Auth ...
type Auth struct {
	Login    string `json:"login,omitempty"`
	Password string `json:"-"`
}
