package app

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// RegisterPlayer ...
// @Summary Регистрация игрока
// @Description Регистрирует игрока
// @Tags accounts
// @Accept json
// @Produce  json
// @Param auth body RegisterPlayerReq true "Логин и пароль"
// @Success 200 {object} string
// @Failure 500 {object} string
// @Router /register [POST]
func (api *Api) RegisterPlayer(ctx *gin.Context) {
	var req RegisterPlayerReq

	if err := ctx.BindJSON(&req); err != nil {
		ctx.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	_, ok := api.Storage.Players[req.Login]
	if ok {
		log.Printf("Пользователь [%s] уже зарегистрирован", req.Login)
		ctx.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	api.Storage.Players[req.Login] = Player{
		Auth: req.Auth,
	}

	ctx.AbortWithStatus(http.StatusOK)
}

type RegisterPlayerReq struct {
	Auth
}

// GetUserInfo ...
// @Summary Получение информации о пользователе
// @Description Возвращает данные пользователя по его логину
// @Tags accounts
// @Accept json
// @Produce  json
// @Param login path string true "Логин"
// @Success 200 {object} GetUserInfoResp
// @Failure 404
// @Router /account/{login} [get]
func (api *Api) GetUserInfo(ctx *gin.Context) {
	var req GetUserInfoReq
	if err := ctx.ShouldBindUri(&req); err != nil {
		log.Printf("Ошибка в запросе")
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	user, ok := api.Storage.Players[req.Login]
	if !ok {
		log.Printf("Такого пользователя нет")
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	ctx.JSON(http.StatusOK, user)
}

type GetUserInfoReq struct {
	Login string `uri:"login" binding:"required"`
}

type GetUserInfoResp struct {
	Player Player `json:"player,omitempty"`
}

// RegisterPlayer ...
// @Summary Редактирование игрока
// @Description Изменяет параметры игрока, все параметры указывать не обязательно
// @Tags accounts
// @Accept json
// @Produce  json
// @Param login path string true "Логин"
// @Param age query string true "Возраст"
// @Param nick query string true "Никнейм"
// @Success 200 {object} string
// @Failure 500 {object} string
// @Router /configure/{login} [POST]
func (api *Api) ConfigurePlayer(ctx *gin.Context) {
	var req ConfigurePlayerReq

	if err := ctx.ShouldBindUri(&req); err != nil {
		log.Printf("Ошибка в запросе")
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	user, ok := api.Storage.Players[req.Login]
	if !ok {
		log.Printf("Такого пользователя нет")
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	if age := ctx.Query("age"); age != "" {
		a, err := strconv.ParseInt(age, 10, 64)
		if err != nil {

			ctx.AbortWithStatus(http.StatusBadRequest)
			log.Printf("Не правильно указан возраст")
			return
		}

		user.Age = int(a)
	}

	if nick := ctx.Query("nick"); nick != "" {
		user.Nick = nick
	}

	api.Storage.Players[user.Login] = user
	ctx.AbortWithStatus(http.StatusOK)
}

type ConfigurePlayerReq struct {
	Login string `uri:"login" binding:"required"`
}
