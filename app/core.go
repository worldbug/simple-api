package app

import (
	"example_api/docs"

	swaggerfiles "github.com/swaggo/files"

	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// Api ...
type Api struct {
	Router  *gin.Engine
	Storage *Storage
}

func (api *Api) pairHandlers() {
	base := api.Router.Group(basePath)
	base.POST("/register", api.RegisterPlayer)
	base.GET("/account/:login", api.GetUserInfo)
	base.POST("/configure/:login", api.ConfigurePlayer)
}

func (api *Api) Start() {
	docs.SwaggerInfo.BasePath = basePath

	api.Router.GET(
		"/swagger/*any",
		ginSwagger.WrapHandler(swaggerfiles.Handler,
			ginSwagger.DefaultModelsExpandDepth(-1),
		),
	)

	api.pairHandlers()
	api.Router.Use(CORSMiddleware())
	api.Router.Run(port)
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
