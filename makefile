generate:
	swag init --parseDependency  --generalInfo main.go

run:
	go mod tidy && go run main.go
